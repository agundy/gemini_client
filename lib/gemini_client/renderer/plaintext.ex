defmodule GeminiClient.Renderer.Plaintext do
  def render(%GeminiClient.Response{status: 20, meta: "text/gemini"} = resp) do
    lines = String.split(resp.body, "\n")

    Enum.each(lines, fn line ->
      case line do
        "=>" <> link ->
          render_link(link)

        line ->
          IO.puts(line)
      end
    end)
  end

  defp render_link(link) do
    [url, link_name] = link |> String.trim() |> String.split(~r{\s}, parts: 2, trim: true)
    IO.puts(link_name)
  end
end
