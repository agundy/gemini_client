defmodule GeminiClient do
  @moduledoc """
  GeminiClient is an Elixir library that supports the Gemini protocol and
  is meant to be a consumer application
  """

  def start() do
    loop()
  end

  defp loop() do
    case String.trim(IO.gets("> ")) do
      "g " <> uri ->
        resp = GeminiClient.Request.get(uri)
        GeminiClient.Renderer.Plaintext.render(resp)
        loop()
    end
  end
end
