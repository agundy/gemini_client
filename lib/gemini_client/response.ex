defmodule GeminiClient.Response do
  defstruct status: 0, meta: "", body: [], uri: %URI{}
end
