# GeminiClient

A hobby Gemini client for education and fun.
[Gemini](https://gemini.circumlunar.space/docs/) is a protocol somewhere
between gopher and the web, and much closer to Gopher.

This repo is for fun exploring building Elixir CLI's and to scratch some itches
playing around in the indie web.

The current project state is profoundly not working. There is a basic broken parser and a plain
text renderer that leaves a lot to be desired. I'll clean it up eventually..
trust me it's on the to do list.

```elixir
iex(1)> GeminiClient.Request.get("gemini://gemini.circumlunar.space/docs/") |> GeminiClient.Renderer.Plaintext.render
# Gemini protocol documentation

## Core documents

        Project Gemini FAQ
Protocol specification
Best practices for Gemini implementers
        Companion specifications

## Resources for beginners

        A quick introduction to "gemtext" markup
Gemtext cheatsheet
A gentle, Gemini-centric guide to TLS certificates

## Translations

Volunteer translations of the above documents into other languages

:ok
```

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `gemini_client` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:gemini_client, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/gemini_client](https://hexdocs.pm/gemini_client).

