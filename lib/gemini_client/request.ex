defmodule GeminiClient.Request do
  def get(uri) do
    uri = URI.parse(uri)
    {:ok, sock} = Socket.SSL.connect(uri.host, uri.port || 1965)
    :ok = Socket.Stream.send!(sock, URI.to_string(uri) <> "\r\n")
    resp = receive_response(sock)
    {status, meta, body} = parse(resp)

    %GeminiClient.Response{
      status: status,
      meta: meta,
      body: body,
      uri: uri
    }
  end

  defp parse(resp) do
    [header, remainder] = String.split(resp, "\r\n", parts: 2)

    [status, meta] = header |> String.trim() |> String.split()
    {String.to_integer(status), meta, remainder}
  end

  defp receive_headers(socket, resp \\ []) do
    [status, meta] =
      case Socket.Stream.recv(socket) do
        {:ok, headers} ->
          headers |> String.trim() |> String.split()
      end

    {String.to_integer(status), meta}
  end

  defp receive_response(socket, resp \\ "") do
    case Socket.Stream.recv(socket) do
      {:ok, nil} ->
        resp

      {:ok, text} ->
        receive_response(socket, resp <> text)

      other ->
        IO.inspect(resp, label: "response")
        {:error, other}
    end
  end
end
